package net.therap.demoConfig.web.filter;

import org.sitemesh.DecoratorSelector;
import org.sitemesh.content.Content;
import org.sitemesh.webapp.WebAppContext;

/**
 * @author mahfuz.ahmed
 * @since 6/27/21
 */
public class UserDecoratorSelector implements DecoratorSelector<WebAppContext> {

    @Override
    public String[] selectDecoratorPaths(Content content, WebAppContext context) {
        return new String[]{
                "/", "/WEB-INF/decorator/decorator.jsp"
        };
    }
}
