package net.therap.demoConfig.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author mahfuz.ahmed
 * @since 6/27/21
 */
@Controller
public class HomeController {

    private static final String VIEW_PAGE = "home";

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String show() {

        return VIEW_PAGE;
    }
}
