<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%--
    @author mahfuz.ahmed
    @since 6/27/21
--%>
<html>
<head>
  <title><c:out value="Home"/></title>
</head>
<body>
  <c:out value="Hello from home.jsp"/>
</body>
</html>
