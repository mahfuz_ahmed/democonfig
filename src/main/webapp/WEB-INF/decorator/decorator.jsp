<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
    @author mahfuz.ahmed
    @since 6/27/21
--%>
<html>
<head>
    <title><sitemesh:write property="title"/></title>
</head>
<body>
    <h2><c:out value="Hello from decorator.jsp"/></h2>

    <sitemesh:write property="body"/>
</body>
</html>
